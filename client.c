#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>

int end = 0;
void* terminal(void* sockfd);
int main(int argc, char *argv[]){

	struct sockaddr_in addr;// = (struct sockaddr_in)malloc(sizeof(struct sockaddr_in));
	struct hostent* host_ip;// = (struct hostent*)malloc(sizeof(struct hostent));
	int sockfd, end = 0,i = 0;
	char hello_msg[47*9] = "";	
	char command[1000] = "";
	char ip[32] = "";
	char* ptr = argv[1];
	char **pptr;
	pthread_t tid;
	int err;
	void *tret;
	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(atoi(argv[2]));
	ptr = argv[1];
	if((host_ip = gethostbyname(ptr)) == NULL){
		printf("Error: gethostbyname");
		return 0;
	}
	if(host_ip->h_addrtype == AF_INET6 || host_ip->h_addrtype == AF_INET){
		pptr = host_ip->h_addr_list;
		for(;*pptr != NULL; pptr++) inet_ntop(host_ip->h_addrtype, *pptr, ip, sizeof(ip));
	}
	else if(host_ip->h_addrtype == AF_INET){;}
	else{
		printf("unknown ip\n");
		return 0;
	}
	inet_pton(AF_INET, ip, &addr.sin_addr);
	
	if( (sockfd = socket(AF_INET,SOCK_STREAM,0) ) == -1){
		printf("socket error\n");
		return 0;
	}
	if( connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr)) == -1){
		printf("connect error\n");
		return 0;
	}

	read(sockfd, hello_msg, sizeof(hello_msg));
	printf("%s\n",hello_msg);

	if( (err = pthread_create(&tid, NULL, terminal, (void*)&sockfd)) != 0) exit(EXIT_FAILURE);
	
	write(1 ,"csh>", 4);
	while(1){
		end = 0;
		memset(command, '\0', 1000);
		gets(command);//read the input command from stdin
		//if(strcmp(command,"setenv") == 0 || strcmp(command,"printenv") == 0)
		write(sockfd, command, 1000 );// write the command to server
		
		if(strcmp(command, "quit") == 0){
			end = 1;
			pthread_cancel(tid);
			break; //if the user would like to quit, then break the loop
		}
		else if(strcmp(command, "") == 0) continue;
	}
	close(sockfd);
	pthread_join(tid, &tret);
	return 0;
}
void* terminal(void* sockfd){
	char output[100000] = "";
	int* read_fd = (int*)sockfd;
	while(end != 1){
		memset(output,'\0',100000);
		read(*read_fd, output,100000 );// read the reponse from server
		write(1, output, 100000);// write output on the screen
	}
	pthread_exit((void*)0 );
}
