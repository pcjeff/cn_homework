#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h> 
#include <sys/types.h>
#include <signal.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

struct link_list{
	int fd;
	int counter;
	struct link_list* next;
};
void swap(char a,char b);
char* check_rewrite(char* cmd);
int test_delay_and_pro(char* cmd, struct link_list** head);
int trim(char* string);
void insert(struct link_list** head,int fd,int count);
int pull_out(struct link_list** head);
void patrol(struct link_list** head); 
int exe_seq(char* command,int r_fd, int o_fd);
int token(char *cmd[], char* seq);
int cmd_token(char* arg[], char* cmd);
int read_cmd(char* command,int con_fd);
int main(int argc, char* argv[]){
	
	char *cmd[100], *arg[100];
	char command[10] = "";
        char hello_msg[47*9] = "*********************************************\n**     Welcome to information server       **\n*********************************************\n**  You are in the directory ras/.         **\n**  The directory includes bin/, text.txt  **\n**  bin/ includes: cat, ls, grep           **\n**  In addition, the server also support   **\n**  the command setenv, printenv           **\n*********************************************";
	
	struct sockaddr_in *addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
	struct sockaddr_in *c_addr = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
	
	addr->sin_family = AF_INET;
	addr->sin_port = htons(8000);
	addr->sin_addr.s_addr = htonl(INADDR_ANY);

	int sockfd = -1, con_fd = -1, end = 0;
	socklen_t len = sizeof(struct sockaddr_in);

	setenv("PATH","bin:.",1); //initial the path here
	if( (sockfd = socket(AF_INET, SOCK_STREAM,0)) == -1){//socket
		printf("Error: socket\n");
		return 0;
	}
	if( bind(sockfd,(struct sockaddr*)addr,sizeof(struct sockaddr_in)) == -1){//bind
		printf("Error: bind\n");
		return 0;
	}
	if( listen(sockfd,10) == -1){//listen
		printf("Error: listen\n");
		return 0;
	}
	
	while(1){
		if( (con_fd = accept(sockfd,(struct sockaddr*) c_addr, &len)) == -1){//accept
			printf("Error: accept\n");
			break;
		}
		else{
			pid_t pid = fork();
			if(pid < 0){
				fprintf(stderr,"Error: Unable to fork");
				exit(EXIT_FAILURE);
			}
			else if(pid == 0){
				//To process the command
				struct link_list* head = NULL;
				char*s;
				int count = 0, i = 0;
				int write_fd = con_fd, read_fd = 0 ,fd = -1;
				write(con_fd, hello_msg, sizeof(hello_msg));//Say hello to client
				dup2(con_fd, STDOUT_FILENO);
				dup2(con_fd, STDERR_FILENO);
				while(1){
					//process the command here
					write_fd = con_fd;
					read_fd = 0;
					fd = -1;
					memset(command, '\0', 1024);// clear the command string to avoid previous instruction remaining in 
					patrol(&head);//every element - 1
					if(read_cmd(command,con_fd) == -1){
						break;//read from client and check if quit
					}
						
					if( (count = test_delay_and_pro(command,&head))){
						int fd[2];
						pipe(fd);
						insert(&head,fd[0],count);
						write_fd = fd[1];
					}
					else if(strstr(command,">") != NULL){
						char* filename = check_rewrite(command);//trunc the command out off > filename
						fd = open(filename, O_CREAT | O_RDWR | O_EXCL | O_APPEND,0666);
						if(fd == -1){
							exit(EXIT_FAILURE);
						}
						write_fd = fd;
					}
					while(1){
						read_fd = pull_out(&head);
						exe_seq(command, read_fd, write_fd);
						if(read_fd == 0) break; 
					}
				}
				close(con_fd);//client close the connection
				exit(EXIT_SUCCESS);
			}
			else if(pid > 0){
				//close the useless con_fd in parent
				close(con_fd);
			}
		}
	}

	return 0;
}
char* check_rewrite(char* cmd){
	char* tmp = NULL;
	int i = 0;
	while(cmd[++i] != '>'){;}
	tmp = cmd+i+2;//point to the char next ot '>'
	for(; i < strlen(cmd) ; i++){
		cmd[i] = '\0';//trunc the "> filename" out off cmd
	}
	return tmp;

}
int read_cmd(char* command,int con_fd){
	read(con_fd,command, 1024); //read the command sended from server

	if(strcmp(command, "quit\n") == 0) return  -1; //the client wants to close the connection
	else return 0;//succeed
}

int token(char *cmd[],char *seq){//separate every cmd in seq and fill it in *cmd[], return size
	char *tokenPtr = NULL;
	int size = 0;
	char *seq_backup = (char*)malloc(strlen(seq) * sizeof(char));//backup the seq, reserve the seq(command) for the next 
	strcpy(seq_backup,seq);
	if( strstr( seq, "|") != NULL){//pipeline 
		tokenPtr = strtok( seq_backup, "|");
		while(tokenPtr != NULL){
			if(isdigit(tokenPtr[0])){;}
			else cmd[size++] = tokenPtr;
			tokenPtr = strtok(NULL, "|");
		}
	}
	else{//single cmd
		cmd[size++] = seq_backup;
	}
	return size;
}

int cmd_token(char* arg[], char* cmd){
	char *tokenPtr = NULL;
	int size = 0;
	tokenPtr = strtok(cmd, " ");
	while(tokenPtr != NULL){
		arg[size++] = tokenPtr;
		tokenPtr = strtok(NULL," \n");
	}
	int i = 0;
	for(i = size ; i < 10 ; i ++) arg[i] = (char*)0;
	return size;
}

int exe_seq(char* command,int r_fd, int o_fd){
	char *cmd[1000];
	int num_of_pipe = token(cmd,command);//token the input_sequence into single cmd
	int i = 0;
	int fd[100][2];
	for(i = 0 ; i < num_of_pipe ; i++){
		if(pipe(fd[i]) == -1){
			printf("Error: pipe\n");
			exit(EXIT_FAILURE);
		}
	}
	for(i = 0 ; i < num_of_pipe ; i++){
		/*process every single cmd here*/
		int fd_in = (i == 0)? r_fd : fd[i-1][0];
		int fd_out = (i == num_of_pipe-1)? o_fd: fd[i][1];
		/* exe_cmd()*/
		exe_cmd(cmd[i],fd_in,fd_out,num_of_pipe,fd);
	}
	for(i = 0 ; i < num_of_pipe ; i++){
		close(fd[i][0]);
		close(fd[i][1]);
	}
	/*for(i = 0 ; i < num_of_pipe ; i++){
		int status;
		wait(&status);
	}*/
	return 0;
}

int exe_cmd(char *s_cmd, int fd_in, int fd_out, int size,int fd[][2]){
	pid_t pid = fork();

	if(pid == -1){
		printf("Error : fork\n");
		exit(EXIT_FAILURE);
	}
	else if(pid == 0 && strstr(s_cmd,"setenv") == NULL){
		/*execute the cmd here*/
		int i = 0;
		if(fd_in != STDIN_FILENO) dup2(fd_in, STDIN_FILENO);// set up the pipe
		if(fd_out != STDOUT_FILENO) dup2(fd_out, STDOUT_FILENO);
		
		for(i = 0 ; i < size ; i++){//close the unnessary fd
			close(fd[i][0]);
			close(fd[i][1]);
		}
		char *arg[10];
		cmd_token(arg,s_cmd);//separate every argument
		if(strcmp(arg[0], "grep") == 0){ //special procedure for grep cmd
			if(arg[1][0] == '\'' && arg[1][strlen(arg[1])-1] == '\''){ 
				arg[1][strlen(arg[1])-1] = '\0';
				arg[1]++;
			}
		}
		if(strcmp(arg[0],"printenv") == 0) {
			printf("%s\n",getenv(arg[1]));
		}
		else if(execvp(arg[0], arg) == -1){//exec the cmd func
			printf("Error exevp\n");
			exit(EXIT_FAILURE);
		}
	}
	else {
		char *arg[10];
		int status;
		cmd_token(arg,s_cmd);
		if(strcmp(arg[0],"setenv") == 0){
			setenv(arg[1],arg[2],1);
		}
		return 0;
	}
	exit(EXIT_FAILURE);
}				
void insert(struct link_list** head,int fd,int count){
	struct link_list* newPtr;
	struct link_list* prePtr;
	struct link_list* curPtr;

	newPtr = (struct link_list*)malloc(sizeof(struct link_list));
	
	newPtr->fd = fd;
	newPtr->counter = count;
	newPtr->next = NULL;

	prePtr = NULL;
	curPtr = *head;
	
	while(curPtr != NULL && count >= curPtr->counter){
		prePtr = curPtr;
		curPtr = curPtr->next;
	}
	if(prePtr == NULL){
		newPtr->next = *head;
		*head = newPtr;
	}else{
		prePtr->next = newPtr;
		newPtr->next = curPtr;
	}
	return ;
}
int pull_out(struct link_list** head){
	struct link_list* tmp;
	tmp = *head;
	if(*head != NULL){
		if((*head)->counter == 0){
			int fd = (*head)->fd;
			(*head) = (*head)->next;
			free(tmp);
			return fd;
		}else{
			return 0;
		}
	}else return 0;
}
void patrol(struct link_list** head){
	struct link_list* tmp = *head;
	while(tmp != NULL){	
		tmp->counter--;
		tmp = tmp->next;
	}
	return ;
}
void swap(char a,char b){
	char tmp;
	tmp = a;
	a = b;
	b = tmp;
	return ;
}
int test_delay_and_pro(char* cmd,struct link_list** head){
	if(isdigit(cmd[strlen(cmd)-1])){//the last cmd is a num
		int count = trim(cmd);
		return count;
	}else return 0;
}
int trim(char* string){
	char* d_cmd;
	int i = strlen(string)-1;

	while(1){
		if(string[i] == '|') break;
		i--;
	}
	d_cmd = string + (++i);
	
	return atoi(d_cmd);
}
